package cn.blogss.camera;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import cn.blogss.camera.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = MainActivity.class.getSimpleName();
    private ActivityMainBinding binding;
    private final String[] needPermissions = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initView();
        initData();
    }

    private void initData() {
        // Android 6.0 and above require dangerous permissions.
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if(!needPermissionGranted()){
                requestPermissions(needPermissions, 1);
            }
        }
    }

    private void initView() {
        binding.btSystemCamera.setOnClickListener(this);
        binding.btCameraPreview.setOnClickListener(this);
        binding.btCameraVideo.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id == R.id.bt_system_camera){    // 系统相机
            startActivity(new Intent(this, SystemCameraActivity.class));
        } else if(id == R.id.bt_camera_preview){    // 相机预览
            startActivity(new Intent(this, CameraPreviewActivity.class));
        } else if(id == R.id.bt_camera_video){    // 视频录制
            startActivity(new Intent(this, CameraPreviewActivity.class));
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean needPermissionGranted(){
        for (String needPermission : needPermissions) {
            if (checkSelfPermission(needPermission) != PackageManager.PERMISSION_GRANTED)
                return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.i(TAG, "requestCode: " + requestCode + ", permissions: " + permissions.toString() + ", grantResults: " + grantResults.toString());
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == 1){
            for(int i=0;i<grantResults.length;i++){
                if(grantResults[i] != PackageManager.PERMISSION_GRANTED){
                    Toast.makeText(this, "You reject \"" + permissions[i] + "\" permission.", Toast.LENGTH_LONG).show();
                    return ;
                }
            }
        }
    }
}
