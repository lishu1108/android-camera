package cn.blogss.camera;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import cn.blogss.camera.databinding.ActivitySystemCameraBinding;

/**
 * @author: Little Bei
 */
public class SystemCameraActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = SystemCameraActivity.class.getSimpleName();
    private ActivitySystemCameraBinding binding;
    private static final String PIC_DIR_NAME = "android_camera";
    private String myPicSaveDir;
    private File imgFile;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySystemCameraBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initView();
        initData();
    }

    private void initView() {

    }

    private void initData() {
        myPicSaveDir = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + PIC_DIR_NAME;
        // Jump to system camera.
        // The old method to open system camera.
        imgFile = createImageFile();
        if(imgFile != null){
            Uri uri = Uri.fromFile(imgFile);
            Log.i(TAG, "uri: " + uri.toString());
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            // Note: If the you specifies a "save uri" then write the data to it's
            // stream. Otherwise, pass back a scaled down version of the bitmap
            // directly in the extras.
            intent.putExtra(MediaStore.EXTRA_OUTPUT,uri);
            startActivityForResult(intent, 1);
        }

        // The new method to open system camera.
//        ActivityResultContracts.TakePicture takePicture = new ActivityResultContracts.TakePicture();
//        ActivityResultLauncher<Uri> launcher = registerForActivityResult(takePicture, result -> {
//
//        });
//        launcher.launch(uri);

    }

    private File createImageFile() {
        String date = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.CHINA).format(new Date());

        File newDir = new File(myPicSaveDir);
        boolean created = true;
        try {
            if(!newDir.exists()){
                created = newDir.mkdir();
            }
            return created ? File.createTempFile(date, ".jpg", newDir) : null;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Note: We specifies a "save uri", so the parameter "data" will be NULL.
        // If we don't specifies a "save uri", we can call data.getExtras().get("data") to get a scaled down Bitmap.
        if(requestCode == 1 && resultCode == RESULT_OK){
            // This way you get the scaled down image.
//            Bundle bundle = data.getExtras();
//            Bitmap thumbnailBitmap = (Bitmap)bundle.get("data");
//            binding.ivThumbnails.setImageBitmap(thumbnailBitmap);

            // This way you get the original image.
            Bitmap originBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            binding.ivOriginImage.setImageBitmap(originBitmap);
        }
    }
}
